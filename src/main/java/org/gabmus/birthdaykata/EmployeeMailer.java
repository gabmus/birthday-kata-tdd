package org.gabmus.birthdaykata;

import java.text.MessageFormat;

public class EmployeeMailer implements Mailer {
    public static final String BIRTHDAY_MESSAGE =
            "To: {0}\n" +
            "Subject: Happy birthday!\n" +
            "\n" +
            "Happy birthday dear {1}!\n";
    @Override
    public void sendMail(String recipientAddress, String message) throws InvalidEmailException {
        if (!isEmailAddress(recipientAddress)) throw new InvalidEmailException();
        else {
            // TODO implement
        }
    }

    public void sendMail(EmployeeInterface employee) throws InvalidEmailException {
        sendMail(
                employee.getEmail(),
                BIRTHDAY_MESSAGE
                        .replace("{0}", employee.getEmail())
                        .replace("{1}", employee.getFirstName())
        );
    }
}
