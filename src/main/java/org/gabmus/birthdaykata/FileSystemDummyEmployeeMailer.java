package org.gabmus.birthdaykata;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class FileSystemDummyEmployeeMailer extends EmployeeMailer {
    private File dirFile;
    public FileSystemDummyEmployeeMailer(String path) throws Exception {
        dirFile = new File(path);
        if (!dirFile.exists()) dirFile.mkdirs();


        ////////////////////////////////////////////////////////
//        if (dirFile.exists()) throw new FileAlreadyExistsException(
//                "The file "+path+" already exists"
//        );
//        if (!dirFile.createNewFile())
//            throw new Exception("Cannot create new file "+path);
//        if (!dirFile.setWritable(true, true))
//            throw new Exception("Cannot set file "+path+" as writable");
//        if (!dirFile.canWrite())
//            throw new Exception("Cannot write file "+path);
    }

    @Override
    public void sendMail(String recipientAddress, String message) throws InvalidEmailException {
        super.sendMail(recipientAddress, message);
        try {
            Files.write(
                    Paths.get(dirFile.getPath()+"/"+recipientAddress),
                    Arrays.asList(message.split("\n")),
                    StandardCharsets.UTF_8
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
