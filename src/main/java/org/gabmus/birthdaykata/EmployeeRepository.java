package org.gabmus.birthdaykata;

public class EmployeeRepository {

    private final Employee[] employeeList;

    public EmployeeRepository(String repositoryStr) {
        String[] repoSplit = repositoryStr.split("\n");
        employeeList = new Employee[repoSplit.length-1];
        for (int i = 1; i < repoSplit.length; i++) {
            employeeList[i-1] = new Employee(repoSplit[i]);
        }
    }

    public Employee[] getEmployeeList() {
        return employeeList.clone();
    }

}
