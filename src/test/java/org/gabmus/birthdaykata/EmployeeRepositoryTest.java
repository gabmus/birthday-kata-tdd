package org.gabmus.birthdaykata;

import com.google.common.io.Resources;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

public class EmployeeRepositoryTest {

    public void assertEmployeeValues(Employee e1, String firstName, String lastName, String email, LocalDate dob) {        Assertions.assertAll(
                () -> assertThat(e1.getFirstName()).isEqualTo(firstName),
                () -> assertThat(e1.getLastName()).isEqualTo(lastName),
                () -> assertThat(e1.getDateOfBirth()).isEqualTo(dob),
                () -> assertThat(e1.getEmail()).isEqualTo(email)
        );
    }

    @Test
    public void createRepository() throws Exception {
        String repoTxt = Resources.toString(
                Resources.getResource("employeeRepositoryTest.txt"),
                StandardCharsets.UTF_8
        );
        EmployeeRepository repo = new EmployeeRepository(repoTxt);
        Employee[] elist = repo.getEmployeeList();

        Assertions.assertAll(
                () -> assertThat(elist).hasSize(2),
                () -> assertEmployeeValues(elist[0],
                        "John", "Doe", "john.doe@foobar.com",
                        LocalDate.parse("1982-10-08")),
                () -> assertEmployeeValues(elist[1],
                        "Mary", "Ann", "mary.ann@foobar.com",
                        LocalDate.parse("1975-09-11"))
        );
    }
}
