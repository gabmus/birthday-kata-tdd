package org.gabmus.birthdaykata;

import java.time.LocalDate;

public interface BirthdayServiceInterface {
    public void sendGreetings(LocalDate date);
}
