package org.gabmus.birthdaykata;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@Tag("Mailer")
public class EmployeeMailerTest {

    @Test
    public void sendMailTest() throws Exception {
        EmployeeInterface mockEmployee = mock(EmployeeInterface.class);
        when(mockEmployee.getEmail()).thenReturn("foo@bar.com");
        when(mockEmployee.getFirstName()).thenReturn("Foo");
        HashMapDummyEmployeeMailer mailer = new HashMapDummyEmployeeMailer();
        mailer.sendMail(mockEmployee);
        assertThat(
                mailer.get("foo@bar.com")
        ).contains(
                "To: foo@bar.com\n" +
                "Subject: Happy birthday!\n" +
                "\n" +
                "Happy birthday dear Foo!"
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "",
            "foobar.com",
            "@foobar.com",
            "foo@",
            "foo@.bar",
            "foo?@bar.com",
            "foo@bar?.com",
            "foo@bar.com?"
    })
    public void sendMailToInvalidAddressTest(String address) throws Exception {
        Mailer mailer = new EmployeeMailer();
        assertThatThrownBy(() -> {
            mailer.sendMail(address, "msg");
        }).isInstanceOf(Mailer.InvalidEmailException.class);
    }
}
