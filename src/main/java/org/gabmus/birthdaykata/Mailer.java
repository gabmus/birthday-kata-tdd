package org.gabmus.birthdaykata;

import java.util.regex.Pattern;

public interface Mailer {

    class InvalidEmailException extends Exception {}

    static final Pattern emailPattern = Pattern.compile("^[A-Za-z0-9+_.-]+@[A-Za-z0-9+_.-]+[.][A-Za-z]+$");

    default boolean isEmailAddress(String address) {
        return emailPattern.matcher(address).matches();
    }

    public void sendMail(String recipientAddress, String message) throws InvalidEmailException;
}
