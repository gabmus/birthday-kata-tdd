package org.gabmus.birthdaykata;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

@Tag("BirthdayService")
public class BirthdayServiceTest {

    private static Employee createMockEmployee(String name, String email, int month, int day) {
        Employee mockEmployee = mock(Employee.class);
        when(mockEmployee.getFirstName()).thenReturn(name);
        when(mockEmployee.getEmail()).thenReturn(email);
        when(mockEmployee.getDateOfBirth()).thenReturn(LocalDate.parse(String.format("%d-%02d-%02d", 1990, month, day)));
        return mockEmployee;
    }

    public static class EmployeeMock extends Employee {

        public EmployeeMock(String s) {
            super(s);
        }

        @Override
        public String getFirstName() {
            return "Foo";
        }

        @Override
        public String getLastName() {
            return "Bar";
        }

        @Override
        public String getEmail() {
            return "foo@bar.com";
        }

        @Override
        public LocalDate getDateOfBirth() {
            return LocalDate.parse("1990-01-01");
        }
    }

    @Test
    public void sendGreetingsTest() throws Exception {
        Employee e1 = createMockEmployee("Foo", "foo@bar.com", 1, 1);
        Employee e2 = createMockEmployee("John", "john@bar.com", 1, 10);
        Employee e3 = createMockEmployee("Lorem", "lorem@ipsum.com", 12, 1);
        Employee e4 = createMockEmployee("Baz", "baz@ipsum.com", 1, 1);
        Employee[] employees = {e1, e2, e3, e4};
        EmployeeRepository mockRepo = mock(EmployeeRepository.class);
        when(mockRepo.getEmployeeList()).thenReturn(employees);
        String[] expectedEmails = {"foo@bar.com", "baz@ipsum.com"};

        HashMapDummyEmployeeMailer hmMailer = new HashMapDummyEmployeeMailer();
        BirthdayService bs = new BirthdayService(mockRepo, hmMailer);
        bs.sendGreetings(LocalDate.parse("2020-01-01"));

        assertThat(hmMailer.getKeys()).containsExactlyInAnyOrder(expectedEmails);
        assertThat(
                hmMailer.get("foo@bar.com")
        ).contains(
                "To: foo@bar.com\n" +
                "Subject: Happy birthday!\n" +
                "\n" +
                "Happy birthday dear Foo!"
        );
        assertThat(
                hmMailer.get("baz@ipsum.com")
        ).contains(
                "To: baz@ipsum.com\n" +
                "Subject: Happy birthday!\n" +
                "\n" +
                "Happy birthday dear Baz!"
        );
    }
}
