package org.gabmus.birthdaykata;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;

public class Employee implements EmployeeInterface {

    private String firstName;
    private String lastName;
    private LocalDate dateofBirth;
    private String email;

    public static Employee[] employeeFactory(String s) {
        return (Employee[])Arrays.asList(s.split("\n")).stream().map(
                line -> new Employee(line)
        ).toArray();
    }

    public Employee(String s) {
        String[] spl = s.split(",");
        lastName = StringUtils.capitalize(spl[0].strip());
        firstName = StringUtils.capitalize(spl[1].strip());
        email = spl[3].strip();
        dateofBirth = LocalDate.parse(spl[2].strip().replace("/", "-"));
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public LocalDate getDateOfBirth() {
        return dateofBirth;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateofBirth=" + dateofBirth +
                ", email='" + email + '\'' +
                '}';
    }
}
