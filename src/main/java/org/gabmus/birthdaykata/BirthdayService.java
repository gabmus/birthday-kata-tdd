package org.gabmus.birthdaykata;

import com.google.inject.Inject;

import java.time.LocalDate;
import java.time.ZoneId;

public class BirthdayService implements BirthdayServiceInterface {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMailer emailService;

    private static boolean dateMatches(LocalDate birthday, LocalDate currentDate) {
        return (
                birthday.getMonthValue() == currentDate.getMonthValue() &&
                (
                        birthday.getDayOfMonth() == currentDate.getDayOfMonth() || (
                                    birthday.getDayOfMonth() == 29 && birthday.getMonthValue() == 2 &&
                                            currentDate.getDayOfMonth() == 28 &&
                                            !currentDate.isLeapYear()
                                )
                )
        );
    }

    @Inject
    public BirthdayService(String employeeRepositoryStr, EmployeeMailer emailService) {
        this.employeeRepository = new EmployeeRepository(employeeRepositoryStr);
        this.emailService = emailService;
    }


    BirthdayService(EmployeeRepository employeeRepository, EmployeeMailer emailService) {
        this.employeeRepository = employeeRepository;
        this.emailService = emailService;
    }

    EmployeeMailer getEmailService() {
        return this.emailService;
    }

    @Override
    public void sendGreetings(LocalDate date) {
        for (EmployeeInterface employee: employeeRepository.getEmployeeList()) {
            if (dateMatches(employee.getDateOfBirth(), date)) {
                try {
                    emailService.sendMail(employee);
                } catch (Mailer.InvalidEmailException e) {
                    System.err.println("Invalid email address: "+employee.getEmail());
                }
            }
        }
    }
}
