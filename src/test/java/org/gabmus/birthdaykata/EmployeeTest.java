package org.gabmus.birthdaykata;

import org.junit.Test;
import org.junit.jupiter.api.Tag;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;

@Tag("Employee")
public class EmployeeTest {

    private static Boolean employeeEquals(EmployeeInterface e1, EmployeeInterface e2) {
        return (
                Arrays.asList("getFirstName", "getLastName", "getDateOfBirth", "getEmail").stream().map(
                        name -> {
                            try {
                                Method m = EmployeeInterface.class.getDeclaredMethod(name);
                                m.setAccessible(true);
                                Boolean res = m.invoke(e1).toString().equals(m.invoke(e2).toString());
                                if (!res) System.err.println("Discrepancy in return value of "+name+"\nExpected:\t"+m.invoke(e2).toString()+"\nFound:\t\t"+m.invoke(e1).toString());
                                return res;
                            } catch (NoSuchMethodException e) { return false; }
                            catch (Exception e) {
                                System.err.println("OTHER ERROR FOR METHOD `"+name+"`? HOW?");
                                return false;
                            }
                        }
                ).reduce(Boolean::logicalAnd).orElse(false)
        );
    }

    @Test
    public void createEmployeeFromString() {
        EmployeeInterface mockEmployee = mock(EmployeeInterface.class);
        when(mockEmployee.getFirstName()).thenReturn("Foo");
        when(mockEmployee.getLastName()).thenReturn("Bar");
        when(mockEmployee.getEmail()).thenReturn("foo@bar.com");
        when(mockEmployee.getDateOfBirth()).thenReturn(LocalDate.parse("1970-01-01"));
        // can't spy final method equals, the commented code below doesn't work
//        EmployeeInterface employee = spy(new Employee("foo,bar,1970/01/01,foo@bar.com"));
//        doReturn(employeeEquals(employee, mockEmployee)).when(employee).equals(mockEmployee);
//        when(mockEmployee.equals(employee)).thenReturn(employeeEquals(employee, mockEmployee));
        EmployeeInterface employee = new Employee("bar,foo,1970/01/01,foo@bar.com");
        assertThat(employeeEquals(employee, mockEmployee)).isTrue();
    }

}
