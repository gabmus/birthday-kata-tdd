package org.gabmus.birthdaykata;

import java.time.LocalDate;

public interface EmployeeInterface {
    public String getFirstName();
    public String getLastName();
    public String getEmail();
    public LocalDate getDateOfBirth();
}
