package org.gabmus.birthdaykata;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

public class HashMapDummyEmployeeMailer extends EmployeeMailer {
    private HashMap<String, String> emails = new HashMap<>();
    public HashMapDummyEmployeeMailer() {
    }

    @Override
    public void sendMail(String recipientAddress, String message) throws InvalidEmailException {
        super.sendMail(recipientAddress, message);
        emails.put(recipientAddress, message);
    }

    public String[] getKeys() {
        return emails.keySet().toArray(new String[0]);
    }

    public String get(String key) throws Exception {
        String toret = emails.get(key);
        if (toret == null) throw new Exception("No email to address "+key);
        return toret;
    }
}
